#include "VS2017ActorComponent.h"

#include "VS2017Character.h"

#include "Net/UnrealNetwork.h"


UVS2017ActorComponent::UVS2017ActorComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}


void UVS2017ActorComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UVS2017ActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    return;
    ENetRole Role = GetOwner()->GetLocalRole();
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! %i %i %s %i"),
           Cast<AVS2017Character>(GetOwner())->UID,
           Role,
           *(GetWorld()->IsServer() ? FString("Server") : FString("Client")),
           Var
    );
}

void UVS2017ActorComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UVS2017ActorComponent, Var);
}

void UVS2017ActorComponent::Go()
{
    ++Var;
}
