#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "VSStruct.h"
#include "VS2017Character.generated.h"


UCLASS(config = Game)
class AVS2017Character : public ACharacter
{
    GENERATED_BODY()

    /** Camera boom positioning the camera behind the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

    /** Follow camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* FollowCamera;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MyComponent, meta = (AllowPrivateAccess = "true"))
    class UVS2017ActorComponent* MyComponent;

public:
    AVS2017Character();

    UPROPERTY(Replicated)
    int32 UID = 0;

    /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    float BaseTurnRate;

    /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    float BaseLookUpRate;

protected:
    void BeginPlay() override;

    void OnConstruction(const FTransform& Transform) override;

    void Tick(float DeltaSeconds) override;

    // Network
    UFUNCTION(Client, Reliable)
    void Client_Test(int32 NewFrame);

    UFUNCTION(Client, Reliable)
    void Client_TestStruct(int32 NewFrame, FVSStruct NewStruct);

    UFUNCTION(Client, Unreliable)
    void Client_TestArray(int32 NewFrame, const TArray<int32> &NewArray);

    UFUNCTION(Client, Reliable)
    void Client_TestArrayStruct(int32 NewFrame, const TArray<FVSStruct> &NewArray);

    int32 Frame = 0;

    UPROPERTY(ReplicatedUsing = OnRep_Frame2)
    int32 Frame2 = 0;

    UPROPERTY(ReplicatedUsing = OnRep_Array2)
    TArray<int32> NewArray2;

    UPROPERTY(ReplicatedUsing = OnRep_ArrayStruct2)
    TArray<FVSStruct> NewArrayStruct2;

    UFUNCTION()
    void OnRep_Frame2();

    UFUNCTION()
    void OnRep_Array2();

    UFUNCTION()
    void OnRep_ArrayStruct2();

    void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
    /** Resets HMD orientation in VR. */
    void OnResetVR();

    /** Called for forwards/backward input */
    void MoveForward(float Value);

    /** Called for side to side input */
    void MoveRight(float Value);

    /**
     * Called via input to turn at a given rate.
     * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void TurnAtRate(float Rate);

    /**
     * Called via input to turn look up/down at a given rate.
     * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void LookUpAtRate(float Rate);

    /** Handler for when a touch input begins. */
    void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

    /** Handler for when a touch input stops. */
    void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

    void OnMouseClicked();

    UFUNCTION(Server, Reliable)
    void Server_MouseClicked();

protected:
    // APawn interface
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
    // End of APawn interface

public:
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const
    {
        return CameraBoom;
    }
    /** Returns FollowCamera subobject **/
    FORCEINLINE class UCameraComponent* GetFollowCamera() const
    {
        return FollowCamera;
    }
};

