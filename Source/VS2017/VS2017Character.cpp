#include "VS2017Character.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Engine/Engine.h"
#include "Engine/Public/Net/UnrealNetwork.h"

#include "VS2017ActorComponent.h"


AVS2017Character::AVS2017Character()
{
    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

    // set our turn rates for input
    BaseTurnRate = 45.f;
    BaseLookUpRate = 45.f;

    // Don't rotate when the controller rotates. Let that just affect the camera.
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Configure character movement
    GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
    GetCharacterMovement()->JumpZVelocity = 600.f;
    GetCharacterMovement()->AirControl = 0.2f;

    // Create a camera boom (pulls in towards the player if there is a collision)
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
    CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

    // Create a follow camera
    FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
    FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
    FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

    // Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
    // are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

    MyComponent = CreateDefaultSubobject<UVS2017ActorComponent>(TEXT("MyComponent"));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AVS2017Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    // Set up gameplay key bindings
    check(PlayerInputComponent);
    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

    PlayerInputComponent->BindAxis("MoveForward", this, &AVS2017Character::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AVS2017Character::MoveRight);

    // We have 2 versions of the rotation bindings to handle different kinds of devices differently
    // "turn" handles devices that provide an absolute delta, such as a mouse.
    // "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
    PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAxis("TurnRate", this, &AVS2017Character::TurnAtRate);
    PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("LookUpRate", this, &AVS2017Character::LookUpAtRate);

    // handle touch devices
    PlayerInputComponent->BindTouch(IE_Pressed, this, &AVS2017Character::TouchStarted);
    PlayerInputComponent->BindTouch(IE_Released, this, &AVS2017Character::TouchStopped);

    // VR headset functionality
    PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AVS2017Character::OnResetVR);

    PlayerInputComponent->BindAction("MouseClick", IE_Pressed, this, &AVS2017Character::OnMouseClicked);
}


void AVS2017Character::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld()->IsServer())
    {
        UID = FMath::RandRange(0, 100);
    }
}

void AVS2017Character::OnConstruction(const FTransform& Transform)
{
    MyComponent->RegisterComponent();
}

void AVS2017Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    ++Frame;
    //++Frame2;

    UWorld* World = GetWorld();
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! %s Tick %i"), *(World->IsServer() ? FString("Server") : FString("Client")), Frame);

    if (World->IsServer()
        //&& Frame % 10 == 0
        )
    {
        for (int i = 0; i < 1; ++i)
        {
            //Client_Test(Frame);

            TArray<FVSStruct> NewArrayStruct;
            TArray<int32> NewArray;
            for (int k = 0; k < 100; ++k)
            {
                FVSStruct NewStruct;
                NewStruct.a = FMath::RandRange(0, 100);
                NewStruct.b = FMath::RandRange(0.0f, 100.0f);
                NewArrayStruct.Add(NewStruct);

                NewArray.Add(FMath::RandRange(0, 100));

                //Client_TestStruct(Frame, NewStruct);
            }

            Client_TestArray(Frame, NewArray);
            //Client_TestArrayStruct(Frame, NewArrayStruct);

            //NewArrayStruct2 = NewArrayStruct;

            //NewArray2 = NewArray;

            //if (NewArray2.Num() < 100)
            //{
            //    NewArray2 = NewArray;
            //}
            //else
            //{
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //    NewArray2.Add(FMath::RandRange(0, 100));
            //}
        }
    }
}

void AVS2017Character::Client_Test_Implementation(int32 NewFrame)
{
    FString MessageStr = FString::Format(TEXT("!!!!!!!!!!!! Client_Test {0} {1} {2}"), { NewFrame, Frame, (GetWorld()->IsServer() ? FString("Server") : FString("Client")) });
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! Client_Test %i %i %s"), NewFrame, Frame, *(GetWorld()->IsServer() ? FString("Server") : FString("Client")));
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, *MessageStr);
}

void AVS2017Character::Client_TestStruct_Implementation(int32 NewFrame, FVSStruct NewStruct)
{
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! Client_TestStruct %i %i %s"), NewFrame, Frame, *NewStruct.ToString());
}

void AVS2017Character::Client_TestArray_Implementation(int32 NewFrame, const TArray<int32> &NewArray)
{
    FString MessageStr = FString::Format(TEXT("!!!!!!!!!!!! Client_TestArrayStruct {0} {1}"), { NewFrame, Frame });
    for (const int32 NewValue : NewArray)
    {
        MessageStr.Append(FString::Format(TEXT(" {0}"), { NewValue }));
    }
    UE_LOG(LogTemp, Log, TEXT("%s"), *MessageStr);
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, *MessageStr);
}

void AVS2017Character::Client_TestArrayStruct_Implementation(int32 NewFrame, const TArray<FVSStruct> &NewArray)
{
    FString MessageStr = FString::Format(TEXT("!!!!!!!!!!!! Client_TestArrayStruct {0} {1}"), { NewFrame, Frame });
    for (const FVSStruct &NewValue : NewArray)
    {
        MessageStr.Append(FString::Format(TEXT(" {0}"), { NewValue.ToString() }));
    }
    UE_LOG(LogTemp, Log, TEXT("%s"), *MessageStr);
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, *MessageStr);
}

void AVS2017Character::OnRep_Frame2()
{
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! OnRep_Frame2 %i %i"), Frame2, Frame);
}

void AVS2017Character::OnRep_Array2()
{
    FString MessageStr;
    for (const int32& NewValue : NewArray2)
    {
        MessageStr.Append(FString::Format(TEXT(" {0}"), { NewValue }));
    }
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! OnRep_Array2 %i %s"), Frame, *MessageStr);
}

void AVS2017Character::OnRep_ArrayStruct2()
{
    FString MessageStr;
    for (const FVSStruct& NewStruct : NewArrayStruct2)
    {
        MessageStr.Append(FString::Format(TEXT(" {0}"), { NewStruct.ToString() }));
    }
    UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! OnRep_Array2 %i %s"), Frame, *MessageStr);
    //UE_LOG(LogTemp, Log, TEXT("!!!!!!!!!!!! OnRep_Array2 %i"), Frame);
}

void AVS2017Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AVS2017Character, Frame2);
    DOREPLIFETIME(AVS2017Character, NewArray2);
    DOREPLIFETIME(AVS2017Character, NewArrayStruct2);
    DOREPLIFETIME(AVS2017Character, UID);
}

void AVS2017Character::OnResetVR()
{
    UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AVS2017Character::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
    Jump();
}

void AVS2017Character::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
    StopJumping();
}

void AVS2017Character::OnMouseClicked()
{
    Server_MouseClicked();
}

void AVS2017Character::Server_MouseClicked_Implementation()
{
    MyComponent->Go();
}

void AVS2017Character::TurnAtRate(float Rate)
{
    // calculate delta for this frame from the rate information
    AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AVS2017Character::LookUpAtRate(float Rate)
{
    // calculate delta for this frame from the rate information
    AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AVS2017Character::MoveForward(float Value)
{
    if ((Controller != NULL) && (Value != 0.0f))
    {
        // find out which way is forward
        const FRotator Rotation = Controller->GetControlRotation();
        const FRotator YawRotation(0, Rotation.Yaw, 0);

        // get forward vector
        const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
        AddMovementInput(Direction, Value);
    }
}

void AVS2017Character::MoveRight(float Value)
{
    if ((Controller != NULL) && (Value != 0.0f))
    {
        // find out which way is right
        const FRotator Rotation = Controller->GetControlRotation();
        const FRotator YawRotation(0, Rotation.Yaw, 0);

        // get right vector 
        const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
        // add movement in that direction
        AddMovementInput(Direction, Value);
    }
}
