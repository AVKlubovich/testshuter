// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "VS2017.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VS2017, "VS2017" );
 