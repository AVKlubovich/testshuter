#pragma once

#include "CoreMinimal.h"
#include "VSStruct.generated.h"


USTRUCT()
struct FVSStruct
{
    GENERATED_BODY()

    UPROPERTY()
    int32 a = 0;

    UPROPERTY()
    float b = 0.0f;

    FString ToString() const
    {
        return FString::Format(TEXT("a = {0}, b = {1}"), {a, b});
    }
};
