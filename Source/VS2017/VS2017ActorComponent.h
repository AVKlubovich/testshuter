#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VS2017ActorComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class VS2017_API UVS2017ActorComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UVS2017ActorComponent();

protected:
    void BeginPlay() override;

public:
    void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    void Go();

private:
    UPROPERTY(Replicated)
    int32 Var = 0;
};
