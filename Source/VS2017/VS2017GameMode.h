// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VS2017GameMode.generated.h"

UCLASS(minimalapi)
class AVS2017GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVS2017GameMode();
};



