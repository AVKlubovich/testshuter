// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "VS2017GameMode.h"
#include "VS2017Character.h"
#include "UObject/ConstructorHelpers.h"

AVS2017GameMode::AVS2017GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
